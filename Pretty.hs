module Pretty where

import Text.PrettyPrint

import Syntax



prettyDef :: Definition -> Doc
prettyDef (Function n typ e) =
    let typeline = case typ of
                       Just t  -> text "fun" <+> text n <+> text ":" <+> prettyPoly t
                       Nothing -> empty
        exprline = text "fun" <+> text n <+> text "=" <+> prettyExpr e
    in typeline $$ exprline

prettyPoly :: Polytype -> Doc
prettyPoly (Poly vs m) =
    text "all" <+> hsep (map prettyTvar vs) <> text "." <+> prettyMono m

prettyTvar :: TypeVar -> Doc
prettyTvar ()