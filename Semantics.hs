module Semantics where

import qualified Data.Map as Map
import qualified Data.Word as NW
import qualified Data.Int as NI
import qualified Data.Array.IArray as Arr
import Data.Float

import Syntax



type Env = [(String,Value)]

data Value
    = VInt8 NI.Int8 | VWord8 NW.Word8
    | VInt32 NI.Int32 | VWord32 NW.Word32
    | VInt64 NI.Int64 | VWord64 NW.Word64
    | VFloat32 Float | VFloat64 Double
    | VArray (Arr.Array Int Value)
    | VClosure Expression Env -- for lambda-bound variables
    | VSuspend Expression Env -- for let-bound variables
    | VRecSuspend String Expression Env -- for rec-bound variables
      deriving (Eq, Show)

type Stack = [Value]
type FrameStack = [(Expression, Env)]
type State = (Stack, Expression, Env, FrameStack)

inject :: Program -> State
inject (Prog defs) =
    ([], Map.findWithDefault [] "main" env, env, [])
    where env = map funToEnv defs
          funToEnv (Function n _ e) = (n, VSuspend e [])

eval :: State -> Stack
eval (stk, [], env, []) = stk
eval (stk, [], env, (code,oldEnv):fs) = eval (evalStep stk code oldEnv fs)
eval (stk, code, env, fs) = eval (evalStep stk code env fs)

evalStep :: Stack -> Expression -> Env -> FrameStack -> State
evalStep stk (cur:next) env fs =
    case cur of
        WNum l -> (numToVal l : stk, next, env, fs)
        WArray size -> (pushArray size stk, next, env, fs)
        WIdent n ->
            case lookup n env of
                Just (VSuspend code env') ->
                    (stk, code, env', (next,env):fs)
                Just (VRecSuspend n code env') ->
                    (stk, code, (n,VRecSuspend n code env'):env', (next,env):fs)
                Just v ->
                    (v:stk, code, env, fs)
                Nothing ->
                    error $ "Could not find variable: " ++ n
        WPrim n -> (evalPrim n stk, next, env, fs)
        WBlock e -> (VClosure e env, next, env, fs)
        WBind ns e ->
            let varcount = length ns
            in (drop varcount stk, e, zip (reverse ns) (take varcount stk) ++ env, (next,env):fs)
        WLet n sus e ->
            (stk, e, (n, VSuspend sus env):env, (next,env):fs)
        WRec n sus e ->
            (stk, e, (n, VRecSuspend n sus env):env, (next,env):fs)
        WMatch clauses ->
            case selectClause clauses stk of
                Just (e,env',newStk) -> (newStk, e, env', (next,env):fs)
                Nothing -> error "No patterns matched in match expression"

numToVal :: NumLit -> Value
numToVal (NLInt8 i) -> VInt8 i
numToVal (NLInt32 i) -> VInt32 i
numToVal (NLInt64 i) -> VInt64 i
numToVal (NLWord8 w) -> VWord8 w
numToVal (NLWord32 w) -> VWord32 w
numToVal (NLWord64 w) -> VWord64 w
numToVal (NLFloat32 f) -> VFloat32 f
numToVal (NLFloat64 f) -> VFloat64 f

-- Arrays are created by taking the top element of the stack as the
-- 'canonical' element and using it in each index of the array
pushArray :: Int -> Stack -> Stack
pushArray size (el:stk) =
    VArray (Arr.listArray (0,size-1) (replicate size el)) : stk

evalPrim :: String -> Stack -> Stack
evalPrim "i8-add" (VInt8 n2:VInt8 n1:stk) = VInt8 (n2 + n1):stk

selectClause :: [([Pattern], Expression)] -> Stack -> Maybe (Expression, Env, Stack)
selectClause opts stk = foldr (tryMatch stk) Nothing opts
    where tryMatch stk Nothing (pats,e) =
            case match pats (take (length pats) stk) of
                Just env -> Just (e, env, drop (length pats) stk)
                Nothing  -> Nothing
          tryMatch _   res     _        = res

match :: [Pattern] -> [Value] -> Maybe Env
match pats vals = foldr matchVal Nothing (zip pats vals)
    where
        matchVal :: (Pattern,Value) -> Maybe Env
        matchVal (PNum m, vnum) = if matchNum m vnum then Just [] else Nothing
        matchVal (PRange l h, vnum) = if matchRange l h vnum then Just [] else Nothing
        matchVal (PArray ls seq rs, VArray arr) = matchArray ls seq rs arr
        matchVal (PWildcard, v) = Just []
        matchVal (PAs n p, v) = fmap ((n,v):) (matchVal (p,v))
        matchNum :: Double -> Value -> Bool
        matchNum expec actual =
            case actual of
                VInt8 a -> fromInteger (toInteger expec) == a
                VInt32 a -> fromInteger (toInteger expec) == a
                VInt64 a -> fromInteger (toInteger expec) == a
                VWord8 a -> fromInteger (toInteger expec) == a
                VWord32 a -> fromInteger (toInteger expec) == a
                VWord64 a -> fromInteger (toInteger expec) == a
                VFloat32 a -> float2Double expec == a
                VFloat64 a -> expec == a
        matchRange :: Double -> Double -> Value -> Bool
        matchRange l h actual =
            case actual of
                VInt8 a -> fromInteger (toInteger l) <= a && fromInteger (toInteger h) >= a
                VInt32 a -> fromInteger (toInteger l) <= a && fromInteger (toInteger h) >= a
                VInt64 a -> fromInteger (toInteger l) <= a && fromInteger (toInteger h) >= a
                VWord8 a -> fromInteger (toInteger l) <= a && fromInteger (toInteger h) >= a
                VWord32 a -> fromInteger (toInteger l) <= a && fromInteger (toInteger h) >= a
                VWord64 a -> fromInteger (toInteger l) <= a && fromInteger (toInteger h) >= a
                VFloat32 a -> float2Double l <= a && float2Double h >= a
                VFloat64 a -> l <= a && h >= a
        matchArray :: [Pattern] -> Maybe String -> [Pattern] -> Arr.Array -> Maybe Env
        matchArray ls seq rs varr = do
            (rem,env) <- match ls (Arr.elems varr)
            (rem',env') <- match (reverse rs) (reverse rem)
            case seq of
                Just n -> return $ (n,Arr.listArray (0,(length rem')-1) rem'):(env' ++ env)
                Nothing -> return $ env' ++ env