module Syntax where

import qualified Data.Word as NW
import qualified Data.Int as NI



data Program
    = Prog [Definition]
      deriving (Eq, Show)

data Definition
    = Function String (Maybe Polytype) Expression
      deriving (Eq, Show)

------------------------------------------------
-- TYPE LEVEL
------------------------------------------------

data Kind
    = KUnknown
    | KInd
    | KSeq
    | KFun Kind Kind
      deriving (Eq, Show)

data Polytype
    = Poly [(String,Kind)] Monotype
      deriving (Eq, Show)

data Monotype
    = TInt8 | TWord8 | TInt32 | TWord32 | TInt64 | TWord64
    | TFloat32 | TFloat64
    | TArray
    | TFun
    | TVar String
    | TDotted Monotype
    | TSeq [Monotype]
    | TTuple [Monotype]
    | TApp Monotype Monotype
      deriving (Eq, Show)

------------------------------------------------
-- TERM LEVEL
------------------------------------------------

type Expression = [Word]

data Word
    = WNum NumLit
    | WArray Int
    | WIdent String
    | WPrim String
    | WBlock Expression
    | WBind [String] Expression
    | WLet String Expression Expression
    | WRec String Expression Expression
    | WMatch [([Pattern],Expression)]
      deriving (Eq, Show)

data NumLit
    = NLInt8 NI.Int8
    | NLInt32 NI.Int32
    | NLInt64 NI.Int64
    | NLWord8 NW.Word8
    | NLWord32 NW.Word32
    | NLWord64 NW.Word64
    | NLFloat32 Float
    | NLFloat64 Double
      deriving (Eq, Show)

data Pattern
    = PNum Double
    | PNumRange Double Double
    | PArray [Pattern] (Maybe String) [Pattern]
    | PWildcard
    | PAs String Pattern
      deriving (Eq, Show)