module Typing where

import qualified Data.Set as Set

import Syntax



type Subst = [(String,Monotype)]

class HasVar a where
    ftv :: a -> Set.Set String
    apply :: Subst -> a -> a

instance HasVar a => HasVar [a] where
    ftv = foldr Set.union Set.empty . map ftv
    apply s = map (apply s)

instance HasVar Monotype where
    ftv (TVar n)    = Set.singleton n
    ftv (TDotted t) = ftv t
    ftv (TSeq ts)   = ftv ts
    ftv (TTuple ts) = ftv ts
    ftv (TApp l r)  = ftv l `Set.union` ftv r
    ftv _           = Set.empty

    apply sub (TVar n) =
        case lookup n sub of
            Just t  -> t
            Nothing -> TVar n
    apply sub (TDotted t) =
        case apply sub t of
            TSeq ts -> TSeq ts
            t'      -> TDotted t'
    apply sub (TArray t) =
        case apply sub t of
            TSeq ts -> TSeq $ map TArray ts
            t'      -> TArray t'
    apply sub (TSeq ts) = TSeq $ flatten $ map (apply sub) ts
    apply sub (TTuple ts) = TTuple $ flatten $ map (apply sub) ts
    apply sub (TApp l r) =
        case apply sub l of
            TSeq ls ->
                case apply sub r of
                    TSeq rs -> TSeq $ zipWith TApp ls rs
                    r' -> TSeq $ zipWith TApp ls (repeat r')
            l' ->
                case apply sub r of
                    TSeq rs -> TSeq $ zipWith TApp (repeat l') rs
                    r' -> TApp l' r'
    apply sub t = t

flatten (TSeq ts:ts') = ts ++ flatten ts'
flatten (t:ts) = t : flatten ts
flatten [] = []

instance HasVar Polytype where
    ftv (Poly vs t) = ftv t `Set.difference` Set.fromList vs

    apply sub (Poly vs t) = Scheme vs (apply (filter (flip notElem vs . fst) sub) t)

composeSubst :: Subst -> Subst -> Subst
composeSubst s1 s2 = map (apply s1) s2 `union` s1

type TypeEnv = [(String,Polytype)]
type KindEnv = [(String,Kind)]