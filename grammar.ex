val bind:

    -> x y z in 1 7 x + ;

let bind:

    let x = 8 7 + in x x + ;

rec bind:

    rec f = 1 f + in f f ;

match expr:

    match
        <a, _, c...> 1 2  => e ;
        (1-3) => e ;
        ps => e ;
    end


lift :: (a -> b) -> <a...> -> <b...>
foldl :: (a -> b -> b) -> b -> <a...> -> b
foldr :: (a -> b -> b) -> b -> <a...> -> b


mapmap :: Functor f => (a -> b) -> <(f a)...> -> <(f b)...>
mapmap = lift (fmap f)

hodor :: Functor f => <(f Int)...> -> <(f Float)...>
hodor = mapmap int-to-float

hodor <Just 1, [1,2,3]> = <Just 1.0, [1.0,2.0,3.0]>




lift int-to-float :: <Int...> -> <Float...>

lul :: (a -> b) -> <a...> -> <b...>
lul f = lift f

lul int-to-float :: <Int...> -> <Float...>


\f : a -> b
    lift :: (a -> b) -> <a...> -> <b...>
    lift f :: <a...> -> <b...>
    lift f <[],[],[]> :: <b...>


lol :: <a...> -> (a -> b) -> <b...>
lol t f = lift f t

lol <[], [], []> :: This is a problem



AT EACH STEP OF INFERENCE, EACH DOTTED TYPE SHOULD CONTAIN AT LEAST ONE TYPE VARIABLE
AND EACH VARIABLE PRESENT IN A DOTTED TYPE IN THE RHS OF THE FUNCTION ARROW SHOULD OCCUR IN THE LHS OF THE FUNCTION ARROW AS WELL

UGH: REQUIRE LIFT, FOLDL, AND FOLDR TO TAKE A GENERALIZED TYPE FROM EITHER A LET, REC, OR TOP-LEVEL DEFINITION

1. FORCE LIFT, FOLDL, AND FOLDR TO TAKE THEIR FUNCTION ARGUMENTS, i.e. LIFT IS NOT A VALUE WITHOUT ITS FUNCTION
2. WHEN GENERALIZED, EVERY TYPE VARIABLE IN THE FUNCTION ARGUMENT'S TYPE MUST BE FREE
3. THE FUNCTION ARGUMENT'S TYPE MUST CONTAIN AT LEAST ONE TYPE VARIABLE



<a,b>... ~ <Int,Float> <String,Double> <c,d>...

a |-> a1,a2,a3
b |-> b1,b2,b3

<a1,b1> <a2,b2> <a3,b3>... ~ <Int,Float> <String,Double> <c,d>...